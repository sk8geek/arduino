//
// Temperature Logger
//
// This project brings together use of a temperature sensor, an RTC, 
// an A/D convertor and a serial EEPROM.
//
// temperature sensor:      LM35
// Op amp (optional?):      TL062
// RTC:                     MCP7940N
// A/D convertor:           MCP3208
// EEPROM:                  25AA256
//
// Note:
//
//      The external A/D convertor can probably be omitted by using
//      a microcontroller with in-built analogue inputs.
//
//      The 25AA256 uses SPI. The 24 series uses I2C so the code
//      would need modifying if they were to be used instead.
//

#define RTC_INTERRUPT 2
#define RTC_ENABLE 4
#define CLOCK 5
#define MOSI 6
#define MISO 7
#define AD_ENABLE 8
#define MEM_ENABLE 9

int pointer = 0x0010;
int sampleBytes = 4;
int interval = 600;
String dayNames[7] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
String monthNames[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

void setup() {
    // put your setup code here, to run once:

    pinMode(A5, INPUT);
    pinMode(RTC_ENABLE, OUTPUT);
    pinMode(AD_ENABLE, OUTPUT);
    pinMode(MEM_ENABLE, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(MOSI, OUTPUT);
    pinMode(MISO, INPUT);
    pinMode(RTC_INTERRUPT, INPUT);
    digitalWrite(RTC_ENABLE, LOW);
    digitalWrite(AD_ENABLE, HIGH);
    digitalWrite(MEM_ENABLE, HIGH);
    digitalWrite(CLOCK, LOW);

  //  rtcWrite(0x8f, 0x00); // write enable, osc continue

  //  rtcWrite(0x8f, 0x80); // write enable, osc stop
  //  // set date/time values
  //  rtcWrite(0x86, 0x25); // year
  //  rtcWrite(0x85, 0x01); // month
  //  rtcWrite(0x84, 0x08); // date
  //  rtcWrite(0x83, 0x04); // day (1-7, Sun-Sat)
  //  rtcWrite(0x82, 0x21); // hour
  //  rtcWrite(0x81, 0x24); // minutes
  //  rtcWrite(0x80, 0x00); // seconds

  //   // alarm 0
  //  rtcWrite(0x87, 0x40); // second alarm
  //  rtcWrite(0x88, 0x80); // minute alarm
  //  rtcWrite(0x89, 0x80); // hour alarm
  //  rtcWrite(0x8a, 0x80); // day alarm

  //  // alarm 1
  //  rtcWrite(0x8b, 0x40); // second alarm
  //  rtcWrite(0x8c, 0x00); // minute alarm
  //  rtcWrite(0x8d, 0x80); // hour alarm
  //  rtcWrite(0x8e, 0x80); // day alarm
   
  //  rtcWrite(0x8f, 0x47); // write protect, osc start

//    initValues();
    
    Serial.begin(9600);
}

void loop() {
    // put your main code here, to run repeatedly:

    int intTemp = analogRead(A5);
    word extTemp = readAD(0);
    word extLight = readAD(1);

    int seconds = rtcRead(0x00);
    int mins = rtcRead(0x01);
    int hours = rtcRead(0x02);
    int day = rtcRead(0x03);
    int date = rtcRead(0x04);
    int month = rtcRead(0x05);
    int year = rtcRead(0x06);
    Serial.print("Date/Time: ");
    Serial.print(dayNames[day - 1]);
    Serial.print(" ");
    Serial.print(decodeBCD(date));
    Serial.print(" ");
    Serial.print(monthNames[decodeBCD(month) -1]);
    Serial.print(" 20");
    Serial.print(decodeBCD(year));
    Serial.print(" ");
    Serial.print(decodeBCD(hours));
    Serial.print(":");
    Serial.print(decodeBCD(mins));
    Serial.print(":");
    Serial.println(decodeBCD(seconds));

    Serial.print("Sensors: ext.light: ");
    Serial.print(extLight);
    Serial.print(", ext.temp: ");
    Serial.print(extTemp);
    Serial.print(", int.temp: ");
    Serial.println(intTemp);

    Serial.print("Memory: ");
    int high = rtcRead(0x20);
    int low = rtcRead(0x21);
    Serial.print("pointer: ");
    Serial.print(high);
    Serial.print("/");
    Serial.print(low);
    Serial.print(" = ");
    Serial.println(readMemory(0));

    int rtcStatus = rtcRead(0x10);
    if (bitRead(rtcStatus, 0) == 1) {
        Serial.println("RTC Alarm 0!");
        rtcRead(0x07);
    }
        
    if (bitRead(rtcStatus, 1) == 1) {
        Serial.println("RTC Alarm 1!");
        rtcRead(0x0b);
    }

    Serial.println("");


//    writeMemory(pointer, extTemp, extLight);
    pointer = pointer + sampleBytes;
    savePointer(pointer);

    delay(60000);

}

void readPointer() {
    long high = rtcRead(0x20);
    int low = rtcRead(0x21);
    pointer = high * 256 + low;
}

void savePointer(word ptr) {
    rtcWrite(0x8f, 0x07); // write enable, osc continue
    rtcWrite(0xA0, highByte(ptr));
    rtcWrite(0xA1, lowByte(ptr));
    rtcWrite(0x8f, 0x47); // write protect, osc start
}


byte readMemory(word addr) {
    digitalWrite(MEM_ENABLE, LOW);
    writeValue(0x03);
    writeValue(highByte(addr));
    writeValue(lowByte(addr));
    byte value = readValue();
    digitalWrite(MEM_ENABLE, HIGH);
    return value;
}


void writeMemory(word addr, word temp, word light) {
    enableMemoryWrite();
    digitalWrite(MEM_ENABLE, LOW);
    writeValue(0x02);
    writeValue(highByte(addr));
    writeValue(lowByte(addr));
    writeValue(highByte(temp));
    writeValue(lowByte(temp));
    writeValue(highByte(light));
    writeValue(lowByte(light));
    digitalWrite(MEM_ENABLE, HIGH);
}


// Read analog input from the MCP3208
word readAD(byte channel) {
    digitalWrite(AD_ENABLE, LOW);
    writeBit(1); // start bit
    writeBit(1); // single/not_diff
    writeBit(bitRead(channel, 2));
    writeBit(bitRead(channel, 1));
    writeBit(bitRead(channel, 0));
    readBit(); // sample
    readBit(); // null
    word value = 0;
    for (int i = 11; i >= 0; i--) {
      bitWrite(value, i, readBit());
    }
    digitalWrite(AD_ENABLE, HIGH);
    digitalWrite(MOSI, LOW);
    return value;
}


int decodeHours(byte bcd) {
    String out = "";
    byte nibble = bcd >> 4;
    nibble = nibble & 0x03;
    out = out + (char)(48 + nibble);
    nibble = bcd & 0x0f;
    out = out + (char)(48 + nibble);
    return out.toInt();
}

int decodeBCD(byte bcd) {
    String out = "";
    byte nibble = bcd >> 4;
    nibble = nibble & 0x0f;
    out = out + (char)(48 + nibble);
    nibble = bcd & 0x0f;
    out = out + (char)(48 + nibble);
    return out.toInt();
}

void rtcWrite(byte addr, byte value) {
    digitalWrite(RTC_ENABLE, HIGH);
    writeValue(addr);
    writeValue(value);
    digitalWrite(RTC_ENABLE, LOW);
}

byte rtcRead(byte addr) {
    byte value = 0;
    digitalWrite(RTC_ENABLE, HIGH);
    writeValue(addr);
    value = readValue();
    digitalWrite(RTC_ENABLE, LOW);
    return value;
}

void initValues() {
    // set pointer to 0x0010
    rtcWrite(0x8f, 0x00); // write enable, osc continue
    rtcWrite(0xA0, 0x00);
    rtcWrite(0xA1, 0x10);
    rtcWrite(0x8f, 0x47); // write protect, osc start
    // check to see if a start time is set
    byte date = readMemory(0x0003);
    if (date > 0) {
        Serial.println("data exists!");
    }
    byte seconds;
    byte minutes;
    byte hours;
    byte month;
    byte year;
    // read the time from the RTC
    digitalWrite(RTC_ENABLE, HIGH);
    writeValue(0x00);
    seconds = readValue();
    minutes = readValue();
    hours = readValue();
    date = readValue();
    date = readValue();
    month = readValue();
    year = readValue();
    digitalWrite(RTC_ENABLE, LOW);
    // write the time to memory
    enableMemoryWrite();
    digitalWrite(MEM_ENABLE, LOW);
    writeValue(0x02); // write instruction
    writeValue(0x00); // high address
    writeValue(0x00); // low address
    writeValue(seconds);
    writeValue(minutes);
    writeValue(hours);
    writeValue(date);
    writeValue(month);
    writeValue(year);
    writeValue(0x02); // set interval to 10 minutes
    writeValue(0x58);
    digitalWrite(MEM_ENABLE, HIGH);
}


void enableMemoryWrite() {
    digitalWrite(MEM_ENABLE, LOW);
    writeValue(0x06);
    digitalWrite(MEM_ENABLE, HIGH);
}

void disableMemoryWrite() {
    digitalWrite(MEM_ENABLE, LOW);
    writeValue(0x04);
    digitalWrite(MEM_ENABLE, HIGH);
}


byte readValue() {
    byte value = 0;
    for (int i = 7; i >= 0; i--) {
      bitWrite(value, i, readBit());
    }
    return value;
}

boolean readBit() {
    digitalWrite(CLOCK, HIGH);
    boolean value = digitalRead(MISO);
    digitalWrite(CLOCK, LOW);
    return value;
}

void writeValue(byte value) {
    for (int i = 7; i >= 0; i--) {
      writeBit(bitRead(value, i));
    }
}

void writeBit(boolean bit) {
    digitalWrite(MOSI, bit);
    digitalWrite(CLOCK, HIGH);
    delayMicroseconds(4);
    digitalWrite(CLOCK, LOW);
}

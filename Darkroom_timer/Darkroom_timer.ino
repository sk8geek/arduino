/* Darkroom Timer 
 *
 * A counter that indicates half second intervals.
 * 
 */

// variables
byte timerUnits = 0;
byte timerTens = 0;
boolean incTimer = false;
int brightness = 1;

// digital pin assignments
int units1 = 2;
int units2 = 3;
int units4 = 4;
int units8 = 7;
int tens1 = 8;
int tens2 = 9;
int tens4 = 10;
int tens8 = 11;
int brightn = 5;
int deciPnt = 6;
// analog pin assignments
int control = 0;

void setup() {
  pinMode(units1, OUTPUT);
  pinMode(units2, OUTPUT);
  pinMode(units4, OUTPUT);
  pinMode(units8, OUTPUT);
  pinMode(brightn, OUTPUT);
  pinMode(deciPnt, OUTPUT);
  pinMode(tens1, OUTPUT);
  pinMode(tens2, OUTPUT);
  pinMode(tens4, OUTPUT);
  pinMode(tens8, OUTPUT);
  //brightness = analogRead(control) / 4;
  analogWrite(brightn, brightness);
  writeDisplay();
  analogWrite(deciPnt, LOW);
  delay(500);
}

void loop() {
  if (incTimer) {
    incrementTimers();
    writeDisplay();
    analogWrite(deciPnt, LOW);
  } else {
    analogWrite(deciPnt, brightness);
  }
  //brightness = analogRead(control) / 4;
  analogWrite(brightn, brightness);
  incTimer = !incTimer;
  delay(500);
}

void incrementTimers() {
  timerUnits ++;
  if (timerUnits == B00001010) {
    timerUnits = 0;
    timerTens ++;
    if (timerTens == B00001010) {
      timerTens = 0;
    }
  }
}

void writeDisplay() {
  setPin(timerUnits, units1, B00000001);
  setPin(timerUnits, units2, B00000010);
  setPin(timerUnits, units4, B00000100);
  setPin(timerUnits, units8, B00001000);
  setPin(timerTens, tens1, B00000001);
  setPin(timerTens, tens2, B00000010);
  setPin(timerTens, tens4, B00000100);
  setPin(timerTens, tens8, B00001000);
}

void setPin(byte timer, int pin, byte mask) {
  if (timer & mask) {
    digitalWrite(pin, HIGH);
  } else {
    digitalWrite(pin, LOW);
  }
}


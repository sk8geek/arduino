/* Temperature Display 
 *
 */

// variables
byte tempUnits = 0;
byte tempTens = 0;
boolean inctemp = false;
int temperature = 0;

// digital pin assignments
int units1 = 2;
int units2 = 3;
int units4 = 4;
int units8 = 7;
int tens1 = 8;
int tens2 = 9;
int tens4 = 10;
int tens8 = 11;
// analog pin assignments
int tempIn = 0;

void setup() {
  pinMode(units1, OUTPUT);
  pinMode(units2, OUTPUT);
  pinMode(units4, OUTPUT);
  pinMode(units8, OUTPUT);
  pinMode(tens1, OUTPUT);
  pinMode(tens2, OUTPUT);
  pinMode(tens4, OUTPUT);
  pinMode(tens8, OUTPUT);
}

void loop() {
  setTemp();
  writeDisplay();
  delay(1000);
}

void setTemp() {
  temperature = analogRead(tempIn) / 2;
  tempTens = temperature / 10;
  tempUnits = temperature - (tempTens * 10);
}

void writeDisplay() {
  setPin(tempUnits, units1, B00000001);
  setPin(tempUnits, units2, B00000010);
  setPin(tempUnits, units4, B00000100);
  setPin(tempUnits, units8, B00001000);
  setPin(tempTens, tens1, B00000001);
  setPin(tempTens, tens2, B00000010);
  setPin(tempTens, tens4, B00000100);
  setPin(tempTens, tens8, B00001000);
}

void setPin(byte temp, int pin, byte mask) {
  if (temp & mask) {
    digitalWrite(pin, HIGH);
  } else {
    digitalWrite(pin, LOW);
  }
}


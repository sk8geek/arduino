/*
 Fade
 
 This example shows how to fade an LED on pin 9
 using the analogWrite() function.
 
 This example code is in the public domain.
 */

int ledOut0 = 3;
int ledOut1 = 5;
int ledOut2 = 6;
int ledOut3 = 9;
int ledOut4 = 10;
int ledOut5 = 11;

int ledLevel0 = 255;
int ledLevel1 = 255;
int ledLevel2 = 255;
int ledLevel3 = 255;
int ledLevel4 = 255;
int ledLevel5 = 255;

boolean ledDim0 = true;
boolean ledDim1 = true;
boolean ledDim2 = true;
boolean ledDim3 = true;
boolean ledDim4 = true;
boolean ledDim5 = true;

int ledWaitLimit0 = 255;
int ledWaitLimit1 = 143;
int ledWaitLimit2 = 79;
int ledWaitLimit3 = 239;
int ledWaitLimit4 = 127;
int ledWaitLimit5 = 63;

int ledWait0 = ledWaitLimit0;
int ledWait1 = ledWaitLimit1;
int ledWait2 = ledWaitLimit2;
int ledWait3 = ledWaitLimit3;
int ledWait4 = ledWaitLimit4;
int ledWait5 = ledWaitLimit5;

int greenBrightness = 0;    // how bright the LED is
int redBrightness = 255;
int fadeInAmount = 1;    // how many points to fade the LED by
int fadeOutAmount = 1;
boolean greenFadeUp = true;

// the setup routine runs once when you press reset:
void setup()  { 
  // declare pin 9 to be an output:
  pinMode(ledOut0, OUTPUT);
  pinMode(ledOut1, OUTPUT);
  pinMode(ledOut2, OUTPUT);
  pinMode(ledOut3, OUTPUT);
  pinMode(ledOut4, OUTPUT);
  pinMode(ledOut5, OUTPUT);
  analogWrite(ledOut0, ledLevel0);
  analogWrite(ledOut1, ledLevel1);
  analogWrite(ledOut2, ledLevel2);
  analogWrite(ledOut3, ledLevel3);
  analogWrite(ledOut4, ledLevel4);
  analogWrite(ledOut5, ledLevel5);
} 

// the loop routine runs over and over again forever:
void loop()  { 
  
  // repeat for each LED
  if (ledWait0 > 0) {
    ledWait0--;
  } else {
    if (ledDim0) {
      if (ledLevel0 < 1) {
        ledWait0 = ledWaitLimit0;
        ledDim0 = false;
      } else {
        ledLevel0 = ledLevel0 / 2;
      }
    } else {
      if (ledLevel0 > 250) {
        ledWait0 = ledWaitLimit0;
        ledDim0 = true;
      } else {
        if (ledLevel0 == 0) {
          ledLevel0 = 1;
        }
        if (ledLevel0 < 128) {
          ledLevel0 = ledLevel0 * 2;
        } else {
          ledLevel0 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  // repeat for each LED
  if (ledWait1 > 0) {
    ledWait1--;
  } else {
    if (ledDim1) {
      if (ledLevel1 < 1) {
        ledWait1 = ledWaitLimit1;
        ledDim1 = false;
      } else {
        ledLevel1 = ledLevel1 / 2;
      }
    } else {
      if (ledLevel1 > 250) {
        ledWait1 = ledWaitLimit1;
        ledDim1 = true;
      } else {
        if (ledLevel1 == 0) {
          ledLevel1 = 1;
        }
        if (ledLevel1 < 128) {
          ledLevel1 = ledLevel1 * 2;
        } else {
          ledLevel1 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  // repeat for each LED
  if (ledWait2 > 0) {
    ledWait2--;
  } else {
    if (ledDim2) {
      if (ledLevel2 < 1) {
        ledWait2 = ledWaitLimit2;
        ledDim2 = false;
      } else {
        ledLevel2 = ledLevel2 / 2;
      }
    } else {
      if (ledLevel2 > 250) {
        ledWait2 = ledWaitLimit2;
        ledDim2 = true;
      } else {
        if (ledLevel2 == 0) {
          ledLevel2 = 1;
        }
        if (ledLevel2 < 128) {
          ledLevel2 = ledLevel2 * 2;
        } else {
          ledLevel2 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  // repeat for each LED
  if (ledWait3 > 0) {
    ledWait3--;
  } else {
    if (ledDim3) {
      if (ledLevel3 < 1) {
        ledWait3 = ledWaitLimit3;
        ledDim3 = false;
      } else {
        ledLevel3 = ledLevel3 / 2;
      }
    } else {
      if (ledLevel3 > 250) {
        ledWait3 = ledWaitLimit3;
        ledDim3 = true;
      } else {
        if (ledLevel3 == 0) {
          ledLevel3 = 1;
        }
        if (ledLevel3 < 128) {
          ledLevel3 = ledLevel3 * 2;
        } else {
          ledLevel3 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  // repeat for each LED
  if (ledWait4 > 0) {
    ledWait4--;
  } else {
    if (ledDim4) {
      if (ledLevel4 < 1) {
        ledWait4 = ledWaitLimit4;
        ledDim4 = false;
      } else {
        ledLevel4 = ledLevel4 / 2;
      }
    } else {
      if (ledLevel4 > 250) {
        ledWait4 = ledWaitLimit4;
        ledDim4 = true;
      } else {
        if (ledLevel4 == 0) {
          ledLevel4 = 1;
        }
        if (ledLevel4 < 128) {
          ledLevel4 = ledLevel4 * 2;
        } else {
          ledLevel4 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  // repeat for each LED
  if (ledWait5 > 0) {
    ledWait5--;
  } else {
    if (ledDim5) {
      if (ledLevel5 < 1) {
        ledWait5 = ledWaitLimit5;
        ledDim5 = false;
      } else {
        ledLevel5 = ledLevel5 / 2;
      }
    } else {
      if (ledLevel5 > 250) {
        ledWait5 = ledWaitLimit5;
        ledDim5 = true;
      } else {
        if (ledLevel5 == 0) {
          ledLevel5 = 1;
        }
        if (ledLevel5 < 128) {
          ledLevel5 = ledLevel5 * 2;
        } else {
          ledLevel5 = 255;
        }
      }
    }
  }
  // end of LED routine
  
  
  analogWrite(ledOut0, ledLevel0);
  analogWrite(ledOut1, ledLevel1);
  analogWrite(ledOut2, ledLevel2);
  analogWrite(ledOut3, ledLevel3);
  analogWrite(ledOut4, ledLevel4);
  analogWrite(ledOut5, ledLevel5);
    
  // wait for 30 milliseconds to see the dimming effect    
  delay(50);                            
}




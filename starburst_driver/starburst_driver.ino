#define BRIGHT 10
#define LATCH 11
#define CLOCK 12
#define DATA 13

#define GAP 500

#define number0 0x08bf
#define letters 0x2209
#define letterS 0x210d

int ascii[96] = {0, 1072, 96, 5454, 5485, 2212, 12953, 64, 57, 15, 11968, 5440, 2048, 
4352, 512, 2176, 2239, 134, 4379, 4367, 4390, 4397, 4413, 2177, 4415, 4399, 1088, 
2112, 640, 10240, 4360, 1185, 443, 4407, 1359, 57, 1103, 4409, 4401, 317, 4406, 
1097, 30, 4784, 56, 8374, 8758, 63, 4403, 575, 4915, 8461, 1089, 62, 2224, 2614, 
10880, 9344, 2185, 57, 8704, 15, 35, 8, 8192, 4632, 4664, 4376, 2318, 4281, 4145, 
8463, 4404, 1024, 14, 1728, 48, 5396, 4624, 4380, 4273, 4899, 4112, 8713, 4152, 
28, 2064, 2580, 10880, 334, 2185, 14345, 1088, 905, 8288, 16383};

String msg = "Merry Christmas! ";
int pointer = 0;

void setup() {
    // put your setup code here, to run once:
    pinMode(BRIGHT, OUTPUT);
    pinMode(LATCH, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(DATA, OUTPUT);

    analogWrite(BRIGHT, 200);
    digitalWrite(LATCH, LOW);
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);

}

void loop() {
    // put your main code here, to run repeatedly:

    int decoded = decode(msg.charAt(pointer));
    writeValue(decoded);
    //writeValue(ascii[msg.charAt(pointer) - 32]);
    delay(GAP);

    pointer++;
    if (pointer > msg.length()) {
        delay(GAP * 4);
        pointer = 0;
    }

}

int decode(char ch) {
    if (ch >= 32 && ch <= 127) {
        return ascii[ch - 32];
    }
    return 0;
}

void writeValue(word value) {
    for (int i = 15; i >= 0; i--) {
      writeBit(bitRead(value, i));
    }
    digitalWrite(LATCH, HIGH);
    digitalWrite(LATCH, LOW);
}

void writeBit(boolean bit) {
    digitalWrite(DATA, bit);
    digitalWrite(CLOCK, HIGH);
    digitalWrite(CLOCK, LOW);
}

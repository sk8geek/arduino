/*
  Interface with MCP7940N RTC using I2C.
*/
#define DATA 2        // digital pin for serial data
#define CLOCK 3       // digital pin for serial clock
#define ACK_WAIT 20   // acknowledge wait 
#define CONTROL 0xDE  // device control address (write)

void setup() {
  dataFree();
  clockFree();

  //set alarm/s
  byte alarm0[] = {0x00, 0x02, 0x00, 0x10, 0x00, 0x00};
  serialWriteSequence(0x0A, alarm0, 6);

  byte alarm1[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  serialWriteSequence(0x11, alarm1, 6);

  byte data[] = {0x48, 0x65, 0x6c, 0x6c, 0x6f};
  serialWriteSequence(0x20, data, 5);

  // set time and date
  serialWrite(0x00, 0x00); // disable oscillator
  serialWrite(0x06, 0x25); // year
  serialWrite(0x05, 0x01); // leap year/month
  serialWrite(0x04, 0x11); // date
  serialWrite(0x03, 0x0E); // Vbat enable/day of week
  serialWrite(0x02, 0x22); // 24 hour/hours
  serialWrite(0x01, 0x00); // minutes

  serialWrite(0x00, 0x80); // enable oscillator/seconds
  // serialWrite(0x07, 0x41); // set MFP to square wave
  serialWrite(0x07, 0x30); // set MFP to alarm interrupt

  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(1000);

  // Serial.print(decodeBCD(serialRead(0x01)));
  // Serial.print(":");
  // Serial.println(decodeBCD(serialRead(0x00) & 0x7F));

  serialReadSequence(0x00, 7);
  serialReadSequence(0x0A, 6);
  serialReadSequence(0x11, 6);
  serialReadSequence(0x20, 5);
  Serial.println("=====================");
}


/*
  Write a value to the given address.
*/
void serialWrite(byte addr, byte value) {
  start(false);
  writeAddress(addr);
  if (writeByte(value)) {
    stop();
  }
  stop();
}

/*
  Read from the given address.
*/
byte serialRead(byte addr) {
    start(false);
    writeAddress(addr);
    dataFree();
    clockFree();
    byte value = retrieveCurrent();
    dataFree();
    return value;
}

/*
  Read a sequence of count bytes starting at the given address.
*/
void serialReadSequence(byte startAddr, int count) {
  int pointer = 0;
  start(false);
  writeAddress(startAddr);
  dataFree();
  clockFree();
  start(true);
  while (pointer < count) {
    Serial.print(readByte(), HEX);
    if (pointer < count - 1) {
      ack();
      Serial.print(",");
    }
    pointer++;
  }
  clockLow();
  stop();
  Serial.println("");
  dataFree();
}

void serialWriteSequence(byte startAddr, byte* data, int count) {
  int pointer = 0;
  start(false);
  writeAddress(startAddr);
  while (pointer < count) {
    writeByte(data[pointer]);
    pointer++;
  }
  clockLow();
  stop();
  dataFree();
}


byte retrieveCurrent() {
    start(true);
    byte value = readByte();
    clockLow();
    stop();
    return value;
}

int decodeBCD(byte bcd) {
    String out = "";
    byte nibble = bcd >> 4;
    nibble = nibble & 0x0f;
    out = out + (char)(48 + nibble);
    nibble = bcd & 0x0f;
    out = out + (char)(48 + nibble);
    return out.toInt();
}

/*
  Start data transfer.
  If the transfer is a read we increment the address.
*/
void start(boolean read) {
    dataLow();
    clockLow();
    // Send control byte
    int control = 0xDE;
    if (read) {
        control++;
    }
    writeByte(control);
}

/*
  Acknowledge
*/
void ack() {
  clockLow();
  dataLow();
  clockFree();
  clockLow();
  dataFree();
}

/*
  Stop data transfer.
*/
void stop() {
    dataLow();
    clockFree();
    dataFree();
}

/*
  Write the address to the bus.
*/
void writeAddress(byte addr) {
    writeByte(addr);
}

/*
  Read an 8 bit byte.
*/
byte readByte() {
    byte data = 0;
    dataFree();
    for (int i = 7; i >= 0; i--) {
        clockLow();
        if (digitalRead(DATA) == 1) {
            bitSet(data, i);
        } else {
            bitClear(data, i);
        }
        clockFree();
    }
    return data;
}

/*
  Write an 8 bit byte.
*/
boolean writeByte(byte value) {
  for (int i = 7; i >= 0; i--) {
      writeBit(bitRead(value, i));
  }
  dataFree();
  clockFree();
  // wait for ACK
  boolean ack = 1;
  int wait = ACK_WAIT;
  while (ack == 0 && wait > 0) {
    ack = digitalRead(DATA);
    wait--;
  }
  clockLow();
  return !ack;
}

/*
  Write a single bit.
*/
void writeBit(boolean bit) {
    if (bit) {
        dataFree();
    } else {
        dataLow();
    }
    clockFree();
    clockLow();
}

/*
  **** LOW LEVEL ****
  These methods are the lowest level. They set the clock/data pins.
*/
void dataLow() {
    pinMode(DATA, OUTPUT);
    digitalWrite(DATA, LOW);
}

void dataFree() {
    pinMode(DATA, INPUT);
    digitalRead(DATA);
}

void clockLow() {
    pinMode(CLOCK, OUTPUT);
    digitalWrite(CLOCK, LOW);
}

void clockFree() {
    pinMode(CLOCK, INPUT);
    digitalRead(CLOCK);
}

//
// Read/write serial EEPROM using I2C protocol
//

#define CLOCK 8
#define DATA 7

#define HOLD 0
#define ACK_WAIT 20

int pointer = 0;
String data = "";
word memPointer = 0x0265;
byte memData = 0;

void setup() {
    // put your setup code here, to run once:
    dataFree();
    clockFree();
    delay(1000);

    storePage(0x026f, "Steven");
    delay(2000);

    retrievePage(0x0272, 3);
    delay(4000);

    
}

void loop() {
    // put your main code here, to run repeatedly:

    retrieveRandom(memPointer);
    delay(1000);
    memPointer++;

    if (memPointer > 1000) {
        memPointer = 0;
    }
    
}

void store(word addr, byte value) {
    start(false);
    writeAddress(addr);
    if (writeByte(value)) {
        stop;
    }
    stop();
}

void storePage(word startAddr, String page) {
    start(false);
    writeAddress(startAddr);
    for (int i = 0; i < page.length(); i++) {
        writeByte(page.charAt(i));
    }
    stop();
}

byte retrieveCurrent() {
    start(true);
    byte value = readByte();
    stop();
    return value;
}

void retrieveRandom(word addr) {
    start(false);
    writeAddress(addr);
    dataFree();
    clockFree();
    memData = retrieveCurrent();
    dataFree();
}

String retrievePage(word startAddr, int count) {
    start(false);
    writeAddress(startAddr);
    dataFree();
    clockFree();
    start(true);
    String data = "";
    for (int i = 0; i < count; i++) {
        data += readByte();
        ack();
    }
    data += readByte();
    stop();
    return data;
}

void dataLow() {
    pinMode(DATA, OUTPUT);
    digitalWrite(DATA, LOW);
}

void dataFree() {
    pinMode(DATA, INPUT);
    digitalRead(DATA);
}

void clockLow() {
    pinMode(CLOCK, OUTPUT);
    digitalWrite(CLOCK, LOW);
}

void clockFree() {
    pinMode(CLOCK, INPUT);
    digitalRead(CLOCK);
}

void start(boolean read) {
    dataLow();
    delayMicroseconds(HOLD);
    clockLow();
    delayMicroseconds(HOLD);
    // Send control byte
    int control = 0xa0;
    if (read) {
        control++;
    }
    writeByte(control);
}

void ack() {
    clockLow();
    dataLow();
    clockFree();
    delayMicroseconds(HOLD);
    clockLow();
}

void stop() {
    dataLow();
    clockFree();
    delayMicroseconds(HOLD);
    dataFree();
}

void writeAddress(word addr) {
    writeByte(highByte(addr));
    writeByte(lowByte(addr));
}

byte readByte() {
    byte data = 0;
    dataFree();
    for (int i = 7; i >= 0; i--) {
        clockLow();
        delayMicroseconds(HOLD);
        if (digitalRead(DATA) == 1) {
            bitSet(data, i);
        } else {
            bitClear(data, i);
        }
        clockFree();
    }
    // Don't ACK
    clockLow();
    delayMicroseconds(HOLD);
//    clockFree();
    return data;
}

boolean writeByte(byte value) {
    for (int i = 7; i >= 0; i--) {
        writeBit(bitRead(value, i));
    }
    dataFree();
    clockFree();
    delayMicroseconds(HOLD);
    // wait for ACK
    boolean ack = 1;
    int wait = ACK_WAIT;
    
    while (ack == 0 && wait > 0) {
        ack = digitalRead(DATA);
        wait++;
    }
    clockLow();
    return !ack;
}

void writeBit(boolean bit) {
    if (bit) {
        dataFree();
    } else {
        dataLow();
    }
    clockFree();
    delayMicroseconds(HOLD);
    clockLow();
    delayMicroseconds(HOLD);
}

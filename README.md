# Arduino Experiments

This is a collection of experiments and projects using Arduino.

I am using the Arduino as an easy to use platform to work with various 
integrated circuits - almost like a software breadboard.  Once I'm 
happy that I understand timing requirements etc then I can write code 
for Microchip PIC devices and use those instead.

The key practice that I'm working on at the moment is serial data 
transfer.
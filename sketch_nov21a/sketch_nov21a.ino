
#define ENABLE 2
#define CLOCK 3
#define MOSI 4
#define MISO 5


void setup() {
    // put your setup code here, to run once:

    pinMode(A5, INPUT);
    pinMode(ENABLE, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(MOSI, OUTPUT);
    pinMode(MISO, INPUT);
    digitalWrite(ENABLE, LOW);
    digitalWrite(CLOCK, LOW);

    // set date and time

//    rtcWrite(0x8f, 0x00);
//    rtcWrite(0x82, 0x49);
//    rtcWrite(0x81, 0x52);

//    rtcWrite(0x8f, 0x00);
//    rtcWrite(0x86, 0x20);
//    rtcWrite(0x85, 0x11);
//    rtcWrite(0x84, 0x21);
//    rtcWrite(0x83, 0x07);
//    rtcWrite(0x82, 0x28);
//    rtcWrite(0x81, 0x20);
//    rtcWrite(0x8f, 0x40);
    
    Serial.begin(9600);

}

void loop() {
    // put your main code here, to run repeatedly:

    int val = analogRead(A5);

    int seconds = rtcRead(0x00);
    int mins = rtcRead(0x01);
    int hours = rtcRead(0x02);
    Serial.print(decodeHours(hours));
    Serial.print(":");
    Serial.print(decodeBCD(mins));
    Serial.print(":");
    Serial.print(decodeBCD(seconds));
    Serial.print(" temp: ");
    Serial.println(val);
    delay(60000);

}

String decodeHours(byte bcd) {
    String out = "";
    byte nibble = bcd >> 4;
    nibble = nibble & 0x0b;
    out = out + (char)(48 + nibble);
    nibble = bcd & 0x0f;
    out = out + (char)(48 + nibble);
    return out;
}

String decodeBCD(byte bcd) {
    String out = "";
    byte nibble = bcd >> 4;
    nibble = nibble & 0x0f;
    out = out + (char)(48 + nibble);
    nibble = bcd & 0x0f;
    out = out + (char)(48 + nibble);
    return out;
}

void rtcWrite(byte addr, byte value) {
    digitalWrite(ENABLE, HIGH);
    writeValue(addr);
    writeValue(value);
    digitalWrite(ENABLE, LOW);
}

byte rtcRead(byte addr) {
    byte value = 0;
    digitalWrite(ENABLE, HIGH);
    writeValue(addr);
    value = readValue();
    digitalWrite(ENABLE, LOW);
    return value;
}


byte readValue() {
    byte value = 0;
    for (int i = 7; i >= 0; i--) {
      bitWrite(value, i, readBit());
    }
    return value;
}

boolean readBit() {
    digitalWrite(CLOCK, HIGH);
    delayMicroseconds(10);
    digitalWrite(CLOCK, LOW);
    return digitalRead(MISO);
}

void writeValue(byte value) {
    for (int i = 7; i >= 0; i--) {
      writeBit(bitRead(value, i));
    }
}

void writeBit(boolean bit) {
    digitalWrite(CLOCK, HIGH);
    digitalWrite(MOSI, bit);
    digitalWrite(CLOCK, LOW);
    delayMicroseconds(4);
}

# Speech Generator

An experiment using an Arduino to drive an old (1980s) SP0256 speech generator.

In order to reduce the wiring between the Arduino and the circuit I use 
a shift register.  Improvements to be made include:

- [ ] Using a capacitor/resistor for register
- [ ] Adding an audio amplifier
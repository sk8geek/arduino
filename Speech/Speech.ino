#define DATA 2
#define CLOCK 3
#define LRQ 4
#define ALD 5
#define SR_RESET 6
#define CLEAR 7
#define DEBUG 1000

void setup() {
    // put your setup code here, to run once:
    pinMode(LRQ, INPUT);
    pinMode(ALD, OUTPUT);
    pinMode(DATA, OUTPUT);
    pinMode(CLEAR, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(SR_RESET, OUTPUT);
    digitalWrite(SR_RESET, LOW);
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);
    digitalWrite(CLEAR, LOW);
    digitalRead(LRQ);
    digitalWrite(ALD, HIGH);
    delayMicroseconds(500);
    digitalWrite(SR_RESET, HIGH);
    digitalWrite(CLEAR, HIGH);
    delay(DEBUG);
}

void loop() {
  // put your main code here, to run repeatedly:

  // zero
  // write(0x2b);
  // write(0x3c);
  // write(0x35);
  // write(0x03);

  // one
  // write(0x2e);
  // write(0x18);
  // write(0x0b);
  // write(0x03);

  // two
  // write(0x0d);
  // write(0x1f);
  // write(0x03);

  // three
  // write(0x1d);
  // write(0x00);
  // write(0x0e);
  // write(0x13);
  // write(0x03);

  // four
  // write(0x28);
  // write(0x28);
  // write(0x3a);
  // write(0x03);

  // five
  // write(0x28);
  // write(0x28);
  // write(0x06);
  // write(0x23);
  // write(0x03);

  // six
  // write(067);
  // write(067);
  // write(014);
  // write(014);
  // write(02);
  // write(010);
  // write(067);
  // write(03);
  
  // seven
  // write(0x37);
  // write(0x37);
  // write(0x07);
  // write(0x07);
  // write(0x23);
  // write(0x0c);
  // write(0x0b);
  // write(0x03);
  
  // eight
  // write(0x14);
  // write(0x03);
  // write(0x0d);
  // write(0x03);

  // nine
  // write(0x38);
  // write(0x06);
  // write(0x0b);
  // write(0x03);

  // Theodore
  write(066);
  write(023);
  // write(0x00);
  write(065);
  write(0x00);
  write(041);
  write(072);
  write(0x03);

  // alarm
  // write(0x20);
  // write(0x2d);
  // write(0x3b);
  // write(0x10);
  // write(0x03);

  // alert
  // write(0x20);
  // write(0x2d);
  // write(0x34);
  // write(0x11);
  // write(0x03);

  // January
  // write(012);
  // write(032);
  // write(032);
  // write(013);
  // write(031);
  // write(073);
  // write(023);
  // write(0x03);

//
//
//    write(0x37);
//    write(0x37);
//    write(0x0d);
//    write(0x13);
//    write(0x02);
//    write(0x23);
//    write(0x07);
//    write(0x07);
//    write(0x38);
//    write(0x03);
//    delay(2000);
//
//// Imogen
//    write(0x0c);
//    write(0x10);
//    write(0x35);
//    write(0x0a);
//    write(0x07);
//    write(0x0b);
//    write(0x03);
//    delay(2000);
//
//// Elliot
//    write(0x07);
//    write(0x3e);
//    write(0x00);
//    write(0x13);
//    write(0x01);
//    write(0x18);
//    write(0x0d);
//    write(0x03);
//    delay(2000);
//    
//    write(0x38);
//    //write(0x06);
//    write(0x35);
//    write(0x03);
//    delay(2000);
//    
//    write(0x19);
//    write(0x07);
//    write(0x07);
//    write(0x37);
//    write(0x37);
//    write(0x03);
    delay(2000);


    
//    write(0x37);
//    write(0x37);
//    write(0x02);
//    write(0x0d);
//    write(0x3b);
//    write(0x02);
//    write(0x0d);
//    write(0x0c);
//    write(0x2c);
//    delay(1000);
}


void write(byte value) {
    do { // wait for load request  
    } while (digitalRead(LRQ) == HIGH);
    writeValue(value);
    digitalWrite(ALD, LOW);
//    delay(DEBUG);
    digitalWrite(ALD, HIGH);
//    delay(DEBUG);
}

void writeValue(byte value) {
    digitalWrite(DATA, HIGH);
    digitalWrite(CLOCK, HIGH);
//    delayMicroseconds(1);
    digitalWrite(CLOCK, LOW);
    for (int i = 7; i >= 0; i--) {
      writeBit(bitRead(value, i));
    }
}

void writeBit(boolean bit) {
    digitalWrite(DATA, bit);
    digitalWrite(CLOCK, HIGH);
    delayMicroseconds(1);
    digitalWrite(CLOCK, LOW);
}

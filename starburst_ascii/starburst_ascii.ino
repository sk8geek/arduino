#define segA 0
#define segB 1
#define segC 2
#define segD 3
#define segE 4
#define segF 5
#define segG 6
#define segH 7
#define segJ 8
#define segK 9
#define segL 10
#define segM 11
#define segN 12
#define segP 13

#define number0 "abcdefhm"
#define number1 "hbc"
#define number2 "abnjed"
#define number3 "abnjcd"
#define number4 "fnjbc"
#define number5 "afnjcd"
#define number6 "afnjcde"
#define number7 "ahm"
#define number8 "abcdefnj"
#define number9 "abcdfnj"

#define letterA "abcefnj"
#define letterB "adbglcj"
#define letterC "afed"
#define letterD "adglbc"
#define letterE "afenjd"
#define letterF "afenj"
#define letterG "afedcj"
#define letterH "febcnj"
#define letterI "glad"
#define letterJ "edcb"
#define letterK "fenhk"
#define letterL "fed"
#define letterM "fecbph"
#define letterN "fepkcb"
#define letterO "abcdef"
#define letterP "feabnj"
#define letterQ "abcdefk"
#define letterR "feajbnk"
#define letterS "apjcd"
#define letterT "agl"
#define letterU "febcd"
#define letterV "femh"
#define letterW "femkcb"
#define letterX "phmk"
#define letterY "phl"
#define letterZ "adhm"

#define lettera "endk"
#define letterb "fekdn"
#define letterc "njed"
#define letterd "bcdmj"
#define lettere "afednh"
#define letterf "afen"
#define letterg "abcdpj"
#define letterh "fenjc"
#define letteri "l"
#define letterj "bcd"
#define letterk "ghkl"
#define letterl "fe"
#define letterm "elcnj"
#define lettern "nek"
#define lettero "enjcd"
#define letterp "fenha"
#define letterq "afbnjk"
#define letterr "ne"
#define letters "apkd"
#define lettert "fend"
#define letteru "ecd"
#define letterv "em"
#define letterw "emkc"
#define letterx "phmk"
#define lettery "gjbcd"
#define letterz "adhm"

#define space ""
#define exclam "fel"
#define quote "fg"
#define hash "glnjdbc"
#define dollar "afglcdnj"
#define percent "mhfc"
#define amp "aphnedk"
#define tick "g"
#define rightTick "p"
#define leftPar "afed"
#define rightPar "abcd"
#define splat "pghmlk"
#define plus "glnj"
#define comma "m"
#define minus "nj"
#define point "k"
#define slash "mh"
#define at "hjbafed"
#define query "fahl"
#define backslash "pk"
#define under "d"
#define colon "gl"
#define semi "gm"
#define less "hk"
#define greater "pm"
#define equal "njd"
#define hat "fab"
#define rightBrace "adpmn"
#define leftBrace "adhjk"
#define pipe "gl"
#define tilde "fpg"
#define del "abcdefghjklmnp"

#define spinnerDuration 8

long count = 0;
long loadingCount = 10;

String ascii[96] = {space, exclam, quote, hash, dollar, percent, amp, tick, leftPar, rightPar, splat, plus, comma, minus, point, slash, number0, number1,
number2, number3, number4, number5, number6, number7, number8, number9, colon, semi, less, equal, greater, query, at, letterA, letterB, letterC, letterD,
letterE, letterF, letterG, letterH, letterI, letterJ, letterK, letterL, letterM, letterN, letterO, letterP, letterQ, letterR, letterS, letterT, letterU,
letterV, letterW, letterX, letterY, letterZ, leftPar, backslash, rightPar, hat, under, rightTick, lettera, letterb, letterc, letterd, lettere, letterf, 
letterg, letterh, letteri, letterj, letterk, letterl, letterm, lettern, lettero, letterp, letterq, letterr, letters, lettert, letteru, letterv, letterw, 
letterx, lettery, letterz, rightBrace, pipe, leftBrace, tilde, del};

// Characters \ and ` break the program causing some other characters to not be displayed
String message = " !\"#$%&\'()*+,-.\/0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_abcdefghijklmnopqrstuvwxyz{|}~";

void setup() {
  // put your setup code here, to run once:
  pinMode(segA, OUTPUT);
  pinMode(segB, OUTPUT);
  pinMode(segC, OUTPUT);
  pinMode(segD, OUTPUT);
  pinMode(segE, OUTPUT);
  pinMode(segF, OUTPUT);
  pinMode(segG, OUTPUT);
  pinMode(segH, OUTPUT);
  pinMode(segJ, OUTPUT);
  pinMode(segK, OUTPUT);
  pinMode(segL, OUTPUT);
  pinMode(segM, OUTPUT);
  pinMode(segN, OUTPUT);
  pinMode(segP, OUTPUT);
  reset();
}

void loop() {
  // put your main code here, to run repeatedly:

//  char letter = message[count];
//  String letterVal = decodeAsciiChar(letter);
//  displayChar(letterVal);
//  delay(400);
//
//  if (count > message.length()) {
//    count = 0;
//  }

  int msgBoundary = message.length() + spinnerDuration;

  if (count < spinnerDuration) {
    spinner();
  } else 
  if (count < msgBoundary) {
      int pointer = count - spinnerDuration;
      char letter = message[pointer];
      String letterVal = decodeAsciiChar(letter);
      displayChar(letterVal);
      delay(800);
 } else {
   count = 0;
 }

  count++;
}

String decodeAsciiChar(char letter) {
  int pointer = letter - 32;
  return ascii[pointer];
}

void displayChar(String letter) {
  reset();
  for (int i = 0; i < letter.length(); i++) {
    setSegment(getSegFromLetter(letter[i]), LOW);
  }
}

int getSegFromLetter(char seg) {
  switch (seg) {
    case 'a': return segA;
    case 'b': return segB;
    case 'c': return segC;
    case 'd': return segD;
    case 'e': return segE;
    case 'f': return segF;
    case 'g': return segG;
    case 'h': return segH;
    case 'j': return segJ;
    case 'k': return segK;
    case 'l': return segL;
    case 'm': return segM;
    case 'n': return segN;
    case 'p': return segP;
  }
}

void spinner() {
  reset();
  flashPair(segG, segL);
  flashPair(segH, segM);
  flashPair(segN, segJ);
  flashPair(segP, segK);
}

void flashPair(int seg1, int seg2) {
  setSegment(seg1, LOW);
  setSegment(seg2, LOW);
  delay(100);
  setSegment(seg1, HIGH);
  setSegment(seg2, HIGH);
}

void reset() {
  for (int i = segA; i <= segP; i++) {
    setSegment(i, HIGH);
  }
}

void setSegment(int segment, boolean level) {
  digitalWrite(segment, level);
}

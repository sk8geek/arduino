// Pins
#define DATA 2
#define CLOCK 3
#define CLEAR 4
#define BDIR 5
#define BC1 6
#define CHIP_RESET 7
#define GATE 8

// Delays
#define ADDR_HOLD 32 // was failing to load address when set to 4
#define DATA_HOLD 8
#define CTRL_HOLD 16
#define DEBUG 0

// Registers
#define A_TONE_FINE 0x00
#define A_TONE_COARSE 0x01
#define B_TONE_FINE 0x02
#define B_TONE_COARSE 0x03
#define C_TONE_FINE 0x04
#define C_TONE_COARSE 0x05
#define NOISE 0x06
#define ENABLE 0x07
#define A_AMPLI 0x08
#define B_AMPLI 0x09
#define C_AMPLI 0x0a
#define ENV_FINE 0x0b
#define ENV_COARSE 0x0c
#define ENV_SHAPE 0x0d
#define PORT_A 0x0e
#define PORT_B 0x0f

// Highest tone frequency is course/fine: 0x00 / 0x0a
// Lowest tone frequency is course/fine:  0x0f / 0xff

byte aCourse = 0x01;
byte aFine = 0x08;

byte noise = 0x08;

void setup() {

    Serial.begin(9600);
    
    // put your setup code here, to run once:
    pinMode(DATA, OUTPUT);
    pinMode(CLOCK, OUTPUT);
    pinMode(CLEAR, OUTPUT);
    pinMode(BDIR, OUTPUT);
    pinMode(BC1, OUTPUT);
    pinMode(CHIP_RESET, OUTPUT);
    pinMode(GATE, OUTPUT);

    digitalWrite(CHIP_RESET, LOW);
    digitalWrite(CLEAR, LOW);
    digitalWrite(GATE, LOW);
    digitalWrite(BDIR, LOW);
    digitalWrite(BC1, LOW);
    digitalWrite(DATA, LOW);
    digitalWrite(CLOCK, LOW);
    delay(400);
    digitalWrite(CHIP_RESET, HIGH);
    delay(1000);
    clear();

    writeData(ENABLE, 0x36);

    // writeData(NOISE, noise);
    writeData(A_AMPLI, 0x07);
    writeData(B_AMPLI, 0x00);
    writeData(C_AMPLI, 0x00);
    writeData(A_TONE_COARSE, aCourse);
    writeData(B_TONE_COARSE, 0x00);
    writeData(C_TONE_COARSE, 0x01);
    writeData(A_TONE_FINE, aFine);
    writeData(B_TONE_FINE, 0x80);
    writeData(C_TONE_FINE, 0x30);
//    // set envelope shape
    writeData(ENV_SHAPE, 0x0a);
//    // set envelope period
    writeData(ENV_COARSE, 0x4);
    writeData(ENV_FINE, 0x08);

    writeData(PORT_A, 0);
}


void loop() {
  // put your main code here, to run repeatedly:


//  delay(1000);

}

void writeData(byte addr, byte data) {
  latchAddr(addr);
  writeByte(data);
}

void latchAddr(byte addr) {
    writeValue(addr);
    digitalWrite(BC1, HIGH);
    digitalWrite(BDIR, HIGH);
    digitalWrite(GATE, HIGH);
    delayMicroseconds(CTRL_HOLD);
    digitalWrite(GATE, LOW);
    digitalWrite(BDIR, LOW);
    digitalWrite(BC1, LOW);
    clear();
}

void writeByte(byte data) {
    writeValue(data);
    digitalWrite(BC1, LOW);
    digitalWrite(BDIR, HIGH);
    digitalWrite(GATE, HIGH);
    delayMicroseconds(CTRL_HOLD);
    digitalWrite(GATE, LOW);
    digitalWrite(BDIR, LOW);
    digitalWrite(BC1, LOW);
    clear();
}

void clear() {
  digitalWrite(CLEAR, LOW);
  digitalWrite(CLEAR, HIGH);
}

void writeValue(byte value) {
    digitalWrite(CLOCK, LOW);
    digitalWrite(DATA, LOW);
    for (int i = 7; i >= 0; i--) {
      writeBit(bitRead(value, i));
    }
    digitalWrite(DATA, LOW);
}

void writeBit(boolean bit) {
    digitalWrite(DATA, bit);
    digitalWrite(CLOCK, HIGH);
    digitalWrite(CLOCK, LOW);
}

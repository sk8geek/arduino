#define DATA_OUT 8

void setup() {
    // put your setup code here, to run once:

    pinMode(DATA_OUT, OUTPUT);

    Serial.begin(9600);

}

void loop() {
    // put your main code here, to run repeatedly:

    send(0x0553);
    send(0x0574);
    send(0x0565);
    send(0x0576);
    send(0x0565);
    send(0x056e);

    delay(4000);

    

}

void send(word addrData) {
    sendStart();
    transmit(addrData);
    transmit(~addrData);
    delayMicroseconds(7998);
}

void transmit(word addrData) {
    for (int i = 11; i >= 0; i--) {
        if (bitRead(addrData, i) == 0) {
            sendZero();
        } else {
            sendOne();
        }
    }
}

void sendStart() {
    for (int i=0; i <= 7; i++) {
        sendBurst();
    }
    delayMicroseconds(4000);
}

void sendZero() {
    sendBurst();
    delayMicroseconds(996);
}

void sendOne() {
    sendBurst();
    delayMicroseconds(2000);
}

void sendBurst() {
    for (int i=0; i < 27; i++) {
        digitalWrite(DATA_OUT, HIGH);
        delayMicroseconds(7);
        digitalWrite(DATA_OUT, LOW);
        delayMicroseconds(7);
    }
}

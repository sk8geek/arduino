/**
 * Send serial data via over an IR link.
 * 
 * Transmitter is a Broadcom HR5P-N4CA-00000 IR diode
 * Receiver is a Vishay TSOP38456
 * 
 * The carrier frequency is 54.3kHz. The bit length is 30 times
 * this frequency. The IR receiver switches after 8 cycles.
 * The resulting baud rate is 1800, and this seems reliable.
 * 
 * I won't be needing non-ASCII characters so I have set the 
 * number of data bits to 7, MSB first, even parity and one 
 * stop bit.
 * 
 */


#define DATA_OUT 8

int HALF_WAVE = 7;
int BIT_WIDTH = 30;
int TRIGGER_CYCLES = 8;

void setup() {
    // put your setup code here, to run once:

    pinMode(DATA_OUT, OUTPUT);

    Serial.begin(9600);

}

void loop() {
    // put your main code here, to run repeatedly:

    send(0x53);
    send(0x74);
    send(0x65);
    send(0x76);
    send(0x65);
    send(0x6e);

    delay(4000);

}

void send(byte data) {
    sendStart();
    transmit(data);
    sendStop();
}

void transmit(byte data) {
    boolean parity = true;
    for (int i = 6; i >= 0; i--) {
        if (bitRead(data, i) == 0) {
            sendBurst();
        } else {
            parity = !parity;
            sendStop();
            if ((i > 0 && bitRead(data, i - 1) == 1) || !parity) {
                delayMicroseconds(TRIGGER_CYCLES * HALF_WAVE);
            }
        }
    }
    if (parity) {
        sendBurst();
    } else {
        sendStop();
        delayMicroseconds(TRIGGER_CYCLES * HALF_WAVE);
    }
}

void sendStart() {
    sendBurst();
}

void sendStop() {
    delayMicroseconds(2 * BIT_WIDTH * HALF_WAVE + 2 * TRIGGER_CYCLES * HALF_WAVE);
}

void sendBurst() {
    for (int i=0; i < BIT_WIDTH; i++) {
        digitalWrite(DATA_OUT, HIGH);
        delayMicroseconds(HALF_WAVE);
        digitalWrite(DATA_OUT, LOW);
        delayMicroseconds(HALF_WAVE);
    }
}

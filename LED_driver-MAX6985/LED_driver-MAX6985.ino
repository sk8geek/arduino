//
// 4 digit LED driver using MAX6958 (I2C interface)
//

#define CLOCK 9
#define DATA 8
#define ACK_WAIT 20

int bright = 0;
boolean countUp = 1;

void setup() {
    // put your setup code here, to run once:
    dataFree();
    clockFree();

    store(0x07, 0x01); // display test

    store(0x04, 0x01); // normal operation
    store(0x01, 0x0f); // enable decode mode for all digits
    
    store(0x20, 0x0e); // digit0
    store(0x21, 0x0f); // digit1
    store(0x22, 0x00); // digit2
    store(0x23, 0x06); // digit3
    
    store(0x02, 0x00); // brightness
    
}

void loop() {
    // put your main code here, to run repeatedly:

    store(0x02, bright); // brightness
    delay(100);
    if (countUp) {
        bright++;
    } else {
        bright--;
    }
    if (bright == 0 || bright == 63) {
        countUp = !countUp;
    }

}

void store(byte addr, byte value) {
    start(false);
    writeAddress(addr);
    if (writeByte(value)) {
        stop();
    }
    stop();
}

byte retrieveCurrent() {
    start(true);
    byte value = readByte();
    stop();
    return value;
}

byte retrieveRandom(byte addr) {
    start(false);
    writeAddress(addr);
    dataFree();
    clockFree();
    byte value = retrieveCurrent();
    dataFree();
    return value;
}



void dataLow() {
    pinMode(DATA, OUTPUT);
    digitalWrite(DATA, LOW);
}

void dataFree() {
    pinMode(DATA, INPUT);
    digitalRead(DATA);
}

void clockLow() {
    pinMode(CLOCK, OUTPUT);
    digitalWrite(CLOCK, LOW);
}

void clockFree() {
    pinMode(CLOCK, INPUT);
    digitalRead(CLOCK);
}

void start(boolean read) {
    dataLow();
    clockLow();
    // Send control byte
    int control = 0x70;
    if (read) {
        control++;
    }
    writeByte(control);
}

void ack() {
    clockLow();
    dataLow();
    clockFree();
    clockLow();
}

void stop() {
    dataLow();
    clockFree();
    dataFree();
}

void writeAddress(byte addr) {
    writeByte(addr);
}

byte readByte() {
    byte data = 0;
    dataFree();
    for (int i = 7; i >= 0; i--) {
        clockLow();
        if (digitalRead(DATA) == 1) {
            bitSet(data, i);
        } else {
            bitClear(data, i);
        }
        clockFree();
    }
    // Don't ACK
    clockLow();
    return data;
}

boolean writeByte(byte value) {
    for (int i = 7; i >= 0; i--) {
        writeBit(bitRead(value, i));
    }
    dataFree();
    clockFree();
    // wait for ACK
    boolean ack = 1;
    int wait = ACK_WAIT;
    
    while (ack == 0 && wait > 0) {
        ack = digitalRead(DATA);
        wait++;
    }
    clockLow();
    return !ack;
}

void writeBit(boolean bit) {
    if (bit) {
        dataFree();
    } else {
        dataLow();
    }
    clockFree();
    clockLow();
}
